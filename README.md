# CSS FLEXBOX AND GRID

> ## Flexbox Layout Module


The CSS Flexbox offers a one-dimensional layout. It helps allocate and align the space among items in a container. 

Before the Flexbox Layout module, there were four layout modes:

- Block, for sections in a webpage
- Inline, for text
- Table, for two-dimensional table data
- Positioned, for the explicit position of an element

The Flexible Box Layout Module makes it easier to design a flexible responsive layout structure without using float or positioning.


To get started you have to define a container element as a grid with display: flex;
<details><summary>Click to see code</summary>

```
<!DOCTYPE html>
<html>
<head>
<style>
.flex-container {
  display: flex;
  flex-wrap: nowrap;
  background-color: DodgerBlue;
}

.flex-container > div {
  background-color: #f1f1f1;
  width: 100px;
  margin: 10px;
  text-align: center;
  line-height: 75px;
  font-size: 30px;
}
</style>
</head>
<body>
<h1>Flexible Boxes</h1>

<div class="flex-container">
  <div>1</div>
  <div>2</div>
  <div>3</div>  
  <div>4</div>
  <div>5</div>
  <div>6</div>  
  <div>7</div>
  <div>8</div>
</div>

</body>
</html>

```
</details>

### Here is Flexbox that was generated from the above code


![hi](https://i.im.ge/2021/08/22/gUaD8.png)


> ## Grid Layout Module

CSS Grid Layout, is a two-dimensional grid-based layout system with rows and columns, making it easier to design web pages without having to use floats and positioning. Like tables, grid layout allow us to align elements into columns and rows.

To get started you have to define a container element as a grid with display: grid, set the column and row sizes with grid-template-columns and grid-template-rows, and then place its child elements into the grid with grid-column and grid-row.

<details><summary>Click to see code</summary>

```

<!DOCTYPE html>
<html>
<head>
<style>
.item1 { grid-area: header; }
.item2 { grid-area: menu; }
.item3 { grid-area: main; }
.item4 { grid-area: right; }
.item5 { grid-area: footer; }

.grid-container {
  display: grid;
  grid-template-areas:
    'header header header header header header'
    'menu main main main right right'
    'menu footer footer footer footer footer';
  grid-gap: 10px;
  background-color: #2196F3;
  padding: 10px;
}

.grid-container > div {
  background-color: rgba(255, 255, 255, 0.8);
  text-align: center;
  padding: 20px 0;
  font-size: 30px;
}
</style>
</head>
<body>


<div class="grid-container">
  <div class="item1">Header</div>
  <div class="item2">Menu</div>
  <div class="item3">Main</div>  
  <div class="item4">Right</div>
  <div class="item5">Footer</div>
</div>

</body>
</html>


```
</details>

### Here is the Grid generated from the above code


![hi](https://i.im.ge/2021/08/22/g5RhF.png)

### Important Points:

- Flexbox works on content while the Grid is based on the layout.
- Grid is made for a two-dimensional layout while Flexbox is for one-dimensional. This means Flexbox can work on either rows or columns at a time, but Grid can work on both.
- Flexbox gives you more flexibility while working on either element (row or column). 
- Grid gives you more flexibility to move around the blocks irrespective of your HTML markup.
- The Flexbox layout is best suited to application components and small-scale layouts, while the Grid layout is designed for larger-scale layouts   that are not linear in design.

## Conclusion

- CSS grids are for 2D layouts. It works with both rows and columns.
- Flexbox works better in one dimension i.e.,either rows or columns.
- CSS Grids help you create the outer layout of the webpage. You can build complex as well responsive designs with this. This is why it is called ‘layout first’.
- Flexbox mostly helps align content & move blocks.
- It will be more time-saving and helpful if you use both at the same time.


### References

[w3 schools css grid](https://www.w3schools.com/css/css_grid.asp)

[w3 schools css flexbox](https://www.w3schools.com/css/css3_flexbox_container.asp)

[geeksforgeeks differences](https://www.geeksforgeeks.org/comparison-between-css-grid-css-flexbox/)

### More about CSS Grid and Flexbox

[For Grid](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout)

[For Flexbox](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Flexbox)



